# LDAP Connector

## Dependencies

This is a simple Java LDAP client that binds to LDAP/AD directory and searches for entries based on given filter.  It produces a JSON array as output format. The JAR archive has the following dependencies, they must be available in the class path of the JVM. The versions are adjusted to libraries contained in mirth connect 3.5.1, except for the `jackson-databind` library,  the originally used 2.5.3 version introduces more than 60 high security vulnerabilities, which are all mitigated in the version 2.9.10.8 used for the ldap connector. The java code is build for JVM 1.8. Therefore, only the JAR file of the LDAP client must be provided in the mirth `custom-lib` folder and not additional dependencies.

```
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-api</artifactId>
			<version>${slf4jVersion}</version>
			<scope>compile</scope>
		</dependency>

		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-reload4j</artifactId>
			<version>${slf4jVersion}</version>
			<scope>compile</scope>
		</dependency>

		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-databind</artifactId>
			<version>${jacksonVersion}</version>
			<scope>compile</scope>
		</dependency>

```

The following libraries must be deployed to the mirth container in the `custom-lib/lib`directory with the ldap connector JAR file:

```
└── custom-lib
    ├── lib
    │   ├── jackson-annotations-2.9.10.jar
    │   ├── jackson-core-2.9.10.jar
    │   └── jackson-databind-2.13.2.1.jar
    │   └── jwks-rsa-0.17.0.jar
    │   └── slf4j-api-1.7.36.jar
    │   └── slf4j-reload4j-1.7.36.jar
    └── ldap-connector-0.0.1-SNAPSHOT.jar
```

## Commands

The main class to call the functions is the `de.legendlime.LdapClient` class. To run the client as a mirth script, the following command sequence has to be provided. The result of the `search` method is a string with an JSON array.

```java
var ldapClient = Packages.de.legendlime.LdapClient;

var config = ldapClient.setConfig(PROVIDER_URL, AUTH_SCHEME, USER_DN, PASSWD); 
var context = ldapClient.bind(config);
ldapClient.setSearchControls(ATTRIBUTES, SEARCH_SCOPE, MAX_ENTRIES, MAX_TIMEOUT);

message = ldapClient.search(context, SEARCH_BASE, SEARCH_FILTER, TRAIL_MODE, PRETTY_PRINT, MULTI_VALUED);
ldapClient.unbind(context);

return message;
```

## Parameters

The following parameters have to be provided:

```java
boolean TRAIL_MODE = false; // if trial mode = yes then only logging no output generated
boolean PRETTY_PRINT = true // formatted JSON output in pretty
boolean MULTI_VALUED = true // support multi-valued attributes (creates value lists in JSON output)

String PROVIDER_URL = "ldaps://openldap:1636"; //ldap and ldaps supported
String AUTH_SCHEME = "simple";

// for AD this might not be a DN, but a user name of user@domain instead
String USER_DN = "distinguished name of user or user@domain for AD";
String PASSWD = "password";
String SEARCH_BASE = "ou=users,dc=legendlime,dc=de";
String SEARCH_FILTER = "(objectClass=inetOrgPerson)";
int SEARCH_SCOPE = 2; // 1 = onelevel search, 2 = subtree search
		
// attribute list to be returned, pass null to searchControls for all attributes of an object
String ATTRIBUTES = {"objectClass", "cn", "sn", "givenName", "mail";
long MAX_ENTRIES = 500 // max number of returned entries
int MAX_TIMEOUT = 5000; // timeout in milliseconds
```

## Results

The LDAP client supports optional multi-valued attributes if the `MULTI_VALUED` switch is set to `true`. When multi-valued attributes are switched on, attribute values in the JSON output are formatted as arrays. If `MULTI_VALUED` is set to `false`, only the first attribute value is taken for the result und presented as a skalar value. The distinguished name `dn` of the object is always a single value. 

Result entry with multi-valued attribute support:

```json
{
  "dn" : "cn=jane.doe,ou=users,dc=legendlime,dc=de",
  "mail" : [ "jane.doe@acme.com" ],
  "givenName" : [ "Jane" ],
  "objectClass" : [ "inetOrgPerson", "posixAccount", "shadowAccount" ],
  "sn" : [ "Doe" ],
  "cn" : [ "jane.doe" ]
}
```

Result entry with single-valued attribute support:

```json
{
  "dn" : "cn=jane.doe,ou=users,dc=legendlime,dc=de",
  "mail" : "jane.doe@acme.com",
  "givenName" : "Jane",
  "objectClass" : "inetOrgPerson",
  "sn" : "Doe",
  "cn" : "jane.doe"
}
```

## Other Informations
When using LDAPS the Root-CA certificate of the LDAP server must be in the list trusted JVM certificates (truststore.jks). 

The module still contains the main method so it can be run as a stand-alone application for testing. 