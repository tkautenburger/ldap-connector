package de.legendlime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class LdapClient {

	private static final Logger logger = LoggerFactory.getLogger(LdapClient.class);
	private static SearchControls ctls = new SearchControls();
	private static DirContext ctx;
	private static Hashtable<String, String> cfg;
	
/*
	public static void main(String[] args) throws JsonProcessingException {

		// if set only logging is done and no JSON result provided. Is good
		// to trial and refine your search filters
		final boolean TRAIL_MODE = false;

		// JSON output in pretty or ot
		final boolean PRETTY_PRINT = true;

		// Support multi-valued attribute values
		final boolean MULTI_VALUED = false;

		final String PROVIDER_URL = "ldaps://openldap:1636";
		final String AUTH_SCHEME = "simple";
		// for AD this might not be a DN, but a user name of user@domain instead
		final String USER_DN = "cn=Manager,dc=legendlime,dc=de";
		final String PASSWD = "*****";
		final String SEARCH_BASE = "ou=users,dc=legendlime,dc=de";
		final String SEARCH_FILTER = "(objectClass=inetOrgPerson)";
		final int SEARCH_SCOPE = 2; // 1 = onelevel search, 2 = subtree search

		// attribute list to be returned, pass null to searchControls for all attributes
		// of an object
		final String[] ATTRIBUTES = { "objectClass", "cn", "sn", "givenName", "mail" };
		final long MAX_ENTRIES = 500L;
		final int MAX_TIMEOUT = 5000;

		// these are the calls to be made out of the mirth Transformer script

		cfg = setConfig(PROVIDER_URL, AUTH_SCHEME, USER_DN, PASSWD);
		ctx = bind(cfg);
		setSearchControls(ATTRIBUTES, SEARCH_SCOPE, MAX_ENTRIES, MAX_TIMEOUT);
		String result = search(ctx, SEARCH_BASE, SEARCH_FILTER, TRAIL_MODE, PRETTY_PRINT, MULTI_VALUED);
		unbind(ctx);

		logger.info(result);
		System.out.println(result);
	}
*/
	public static Hashtable<String, String> setConfig(String providerUrl, String authScheme, String userDn,
			String password) {
		cfg = new Hashtable<String, String>();
		cfg.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		cfg.put(Context.PROVIDER_URL, providerUrl);
		cfg.put(Context.SECURITY_AUTHENTICATION, authScheme);
		cfg.put(Context.SECURITY_PRINCIPAL, userDn);
		cfg.put(Context.SECURITY_CREDENTIALS, password);
		return cfg;
	}

	public static DirContext bind(Hashtable<String, String> cfg) {
		try {
			ctx = new InitialDirContext(cfg);
			return ctx;
		} catch (NamingException e) {
			logger.error("Could not connect to directory, reason: {}", e.getMessage());
			if (logger.isDebugEnabled()) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static void unbind(DirContext ctx) {
		if (ctx != null) {
			try {
				ctx.close();
			} catch (NamingException e) {
				logger.error("Could not unbind from directory, reason: {}", e.getMessage());
				if (logger.isDebugEnabled()) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void setSearchControls(String[] attr, int scope, Long count, int millis) {
		ctls.setCountLimit(count);
		ctls.setReturningAttributes(attr);
		ctls.setReturningObjFlag(true);
		ctls.setTimeLimit(millis);
		// OBJECT_SCOPE = 0
		// ONELEVEL_SCOPE = 1
		// SUBTREE_SCOPE = 2
		ctls.setSearchScope(scope);
	}

	private static SearchControls getSearchControls() {
		return ctls;
	}

	public static String search(DirContext ctx, String base, String filter, boolean trail, boolean pretty, boolean mv) {
		ArrayList<HashMap<String, Object>> resultList = new ArrayList<HashMap<String, Object>>();
		try {
			if (ctx != null) {
				NamingEnumeration<SearchResult> result = ctx.search(base, filter, getSearchControls());
				while (result.hasMore()) {
					SearchResult entry = result.next();
					if (trail) {
						logEntry(entry);
					} else {
						resultList.add(entryToMap(entry, mv));
					}
				}
				ObjectMapper mapper = new ObjectMapper();
				if (pretty)
					return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(resultList);
				else
					return mapper.writeValueAsString(resultList);
			} else {
				logger.error("Could not find directory context");
			}
		} catch (NamingException e) {
			logger.error("Directory search failed, reason: {}", e.getMessage());
			if (logger.isDebugEnabled()) {
				e.printStackTrace();
			}
		} catch (JsonProcessingException e) {
			logger.error("Cannot serialize entry to JSON: {}", e.getMessage());
			if (logger.isDebugEnabled()) {
				e.printStackTrace();
			}
		}
		return null;
	}

	private static void logEntry(SearchResult entry) {
		logger.info("object: " + entry.getNameInNamespace());
		try {
			NamingEnumeration<? extends Attribute> allAttributes = entry.getAttributes().getAll();
			while (allAttributes.hasMore()) {
				Attribute attribute = allAttributes.next();
				logger.info("attribute name: {}", attribute.getID());

				NamingEnumeration<? extends Object> allValues = attribute.getAll();
				while (allValues.hasMore()) {
					Object value = allValues.next();
					logger.info("value: {}", value.toString());
				}
			}
		} catch (NamingException e) {
			logger.error("Cannot retrieve object attributes, reason: {}", e.getMessage());
			if (logger.isDebugEnabled()) {
				e.printStackTrace();
			}
		}
	}

	private static HashMap<String, Object> entryToMap(SearchResult entry, boolean mv) {
		HashMap<String, Object> entryMap = new HashMap<String, Object>();
		entryMap.put("dn", entry.getNameInNamespace());
		try {
			NamingEnumeration<? extends Attribute> allAttributes = entry.getAttributes().getAll();
			while (allAttributes.hasMore()) {
				Attribute attribute = allAttributes.next();
				if (mv) {
					NamingEnumeration<? extends Object> allValues = attribute.getAll();
					ArrayList<Object> values = new ArrayList<Object>();
					while (allValues.hasMore()) {
						Object value = allValues.next();
						values.add(value);
					}
					entryMap.put(attribute.getID(), values);
				} else {
					entryMap.put(attribute.getID(), attribute.get());
				}
			}
			return entryMap;
		} catch (NamingException e) {
			logger.error("Cannot retrieve object attributes, reason: {}", e.getMessage());
			if (logger.isDebugEnabled()) {
				e.printStackTrace();
			}
		}
		return null;
	}

}
